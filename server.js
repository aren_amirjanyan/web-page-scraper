const request = require('request'),
  cheerio = require('cheerio');

const HTTPS = false;  //change to tru if the site is HTTPS
const SITE_DOMAIN = 'SITE_DOMAIN'; //please put the site domain name
const SITE_URL = HTTPS ? `https://${SITE_DOMAIN}` : `http://${SITE_DOMAIN}`;
let links = {};

/**
 * Find Page urls
 * @param prevPage
 * @param currentPage
 * @param nextPage
 * @returns {Promise<unknown>}
 */
const findPage = (prevPage, currentPage, nextPage) => {
  
  //put current page in links object
  links[currentPage] = currentPage;
  
  return new Promise((resolve, reject) => {
    request(currentPage, (error, response, body) => {
      
      if (prevPage === currentPage && currentPage === nextPage && prevPage === nextPage) {
        resolve({prevPage, currentPage, nextPage: null, status: response.statusCode});
      }
      if (!error) {
        //load HTML boady with currentPage
        const $ = cheerio.load(body);
        const anchorElemet = $("a");
        if (!anchorElemet.length) {
          resolve({prevPage, currentPage, nextPage: prevPage, status: response.statusCode});
        }
        else {
          let key = 0;
          nextPage = $(anchorElemet[key]).attr("href");
          
          while (currentPage === nextPage || nextPage === '#' || links[nextPage] || !nextPage.includes(SITE_DOMAIN)) {
            key++;
            nextPage = $(anchorElemet[key]).attr("href");
            if (!nextPage) {
              resolve({prevPage, currentPage, nextPage: SITE_URL, status: response.statusCode});
              break;
            }
            else {
              if (!/(http:\/\/|https:\/\/|www\.)\S+/i.test(nextPage) && !nextPage.includes(SITE_DOMAIN) && !nextPage.includes('mailto:')) {
                nextPage = SITE_URL + '/' + nextPage
              }
            }
          }
          resolve({prevPage, currentPage, nextPage: nextPage, status: response.statusCode});
        }
      }
      else {
        resolve({prevPage, currentPage, nextPage: currentPage, status: 404});
      }
    });
  });
};

const init = async (currentPage) => {
  let prevPage = null,
    nextPage = null;
  while (currentPage) {
    let response = await findPage(prevPage, currentPage, nextPage);
    console.log(response);
    currentPage = response.nextPage;
    nextPage = response.nextPage;
    prevPage = response.currentPage;
  }
  if (!currentPage) {
    return 'Finished the searching of the pages!';
  }
};
init(SITE_URL).then(response => {
  console.log(response);
});
